#!/bin/bash

#-------------------------------------------------------------------------------
# Kevin T'Syen dotfiles
#-------------------------------------------------------------------------------

alias php='/Applications/MAMP/bin/php/php5.4.4/bin/php'
alias install_symfony='~/mandrill/puppet/modules/tools/files/install_symfony.sh'
#import ssh color so we indicate when we are on the nginx
alias ssh="~/dotfiles/.ssh_color"

#alias for console for symfony
alias c='./app/console'
alias ct='./app/console --connection=test'

# Basics
: ${HOME=~}
: ${LOGNAME=$(id -un)}
: ${UNAME=$(uname)}

# Fucking mail notifications
unset MAILCHECK

# Proper locale
: ${LANG:="en_US.UTF-8"}
: ${LANGUAGE:="en"}
: ${LC_CTYPE:="en_US.UTF-8"}
: ${LC_ALL:="en_US.UTF-8"}
export LANG LANGUAGE LC_CTYPE LC_ALL

# editor
export EDITOR=vim

#-------------------------------------------------------------------------------
# Path
#-------------------------------------------------------------------------------
# export PATH=/usr/local/bin:/usr/local/sbin:$PATH
export PATH=/usr/local/bin:$PATH

#-------------------------------------------------------------------------------
# Aliases
#-------------------------------------------------------------------------------

# shell
alias ll='ls -lah'
alias la='ls -lah'
alias dir='ls -ld */'

# git
alias g='git add . && git commit && git push'
alias gti='git'
alias gs='git status'
alias gd='git diff'

# vagrant
alias v='vagrant'
alias vs='~/dotfiles/.vagrant_color'
alias vr='vagrant ssh -c'
alias vu='vagrant up'
alias vh='vagrant halt'

# editor aliases
if [ ! -f "/usr/local/bin/subl" ]; then 
	if [ -f /Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl ]; then
		ln -s "/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl" /usr/local/bin/subl
	fi
fi
alias s='sublime'

# mandrill alias
alias mandrill='cd ~/mandrill/srv'

#nginx ip
#alias nginx='appstrakt@62.213.197.23'

# misc
alias password='php -r "\$password=PHP_EOL;for(\$length=0;\$length<12;\$length++){\$password.=chr(rand(40,122));}echo \$password.PHP_EOL.PHP_EOL;"'
alias tprofile="curl -w '\nLookup time:\t%{time_namelookup}\nConnect time:\t%{time_connect}\nPreXfer time:\t%{time_pretransfer}\nStartXfer time:\t%{time_starttransfer}\n\nTotal time:\t%{time_total}\n' -o /dev/null -s "

# composer
alias get_composer='curl -s getcomposer.org/installer | php -d detect_unicode=Off'

# navigation
alias ..='cd ..'
alias ....='cd ../..'
alias ......='cd ../../..'
alias dev='cd ~/mandrill/srv'

#-------------------------------------------------------------------------------
# AMAZON
#-------------------------------------------------------------------------------
if [ -d $HOME/.aws ]; then
	export JAVA_HOME="$(/usr/libexec/java_home)"
	export EC2_PRIVATE_KEY="$(/bin/ls $HOME/.aws/pk-*.pem)"
	export EC2_CERT="$(/bin/ls $HOME/.aws/cert-*.pem)"
	export AWS_RDS_HOME="/usr/local/Cellar/rds-command-line-tools/1.3.003/jars"
	export EC2_REGION="eu-west-1"
fi

#-------------------------------------------------------------------------------
# Autocompleters
#-------------------------------------------------------------------------------
for name in $HOME/dotfiles/autocomplete/*; do
	. $name
done

# brew autocompleters (if any)
# install "brew install bash-completion" for this
if [ `which brew` ]; then
	if [ -f $(brew --prefix)/etc/bash_completion ]; then
		. $(brew --prefix)/etc/bash_completion
	else
		echo "Dude, you should install"
		echo "  brew install bash-completion"
	fi
else
	if [ $(uname) == "Darwin" ]; then
		echo "Dude, you should install brew on a mac, check it out here:"
		echo "  http://mxcl.github.com/homebrew/"
	fi
fi

#-------------------------------------------------------------------------------
# Prompt
#-------------------------------------------------------------------------------
export LSCOLORS="gxcxfxdxbxegedabagacad"
export CLICOLOR=1
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUPSTREAM=true
export PROMPT_COMMAND="echo -n \[\$(date +%H:%M:%S)\]--"
GIT_STATE=$(__git_ps1 " | %s")
YELLOW="\[\e[33;1m\]"
GREEN="\[\e[32;1m\]"
BLUE="\[\e[34;1m\]"
GREY="\[\e[0m\]"
LIGHT_CYAN="\[\033[1;36m\]"

if [ $(whoami) == "vagrant" ]; then
	 export PS1='\[\033[1;36m\]\u@\h \[\e[32;1m\]\w\[\e[34;1m\]$(__git_ps1 " | %s") \[\033[1;36m\]→ \[\e[0m\]'
else
     export PS1='\[\e[33;1m\]\u@\h \[\e[32;1m\]\w\[\e[34;1m\]$(__git_ps1 " | %s") \[\e[32;1m\]→ \[\e[0m\]'
fi

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
# Edit bash_profile
ebash(){
	$EDITOR ~/.bashrc
}

# Edit the hosts file
ehosts(){
	sudo $EDITOR /etc/hosts
}

# Add a host do the hosts file
ahost(){
	sudo tee -a /etc/hosts <<EOF
127.0.0.1	$1 
EOF

	sudo tee -a /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf <<EOF

<VirtualHost *:80>
	DocumentRoot "$2"
	ServerName $1
	ErrorLog "$2logs/apache.$1.error.log"
	CustomLog "$2logs/apache.$1.access.log" common
	php_flag log_errors on
	php_flag display_errors on
	php_value error_reporting 2147483647
	php_value error_log "$2logs/php.$1.error.log"
</VirtualHost>
EOF

	restartsrv
}

# Start apache
startsrv(){
	# /bin/sh
	sudo /Applications/MAMP/bin/startApache.sh
}

# Stop apache
stopsrv(){
	# /bin/sh
	sudo /Applications/MAMP/bin/stopApache.sh 
}

# Restart apache
restartsrv(){
	# /bin/sh
	echo Restarting apache
	sudo /Applications/MAMP/bin/apache2/bin/apachectl restart
}

# Edit http.conf
evhosts(){
	$EDITOR /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf
}

ehttpd(){
        $EDITOR /Applications/MAMP/conf/apache/httpd.conf
}


# Text color variables
txtund=$(tput sgr 0 1)          # Underline
txtbld=$(tput bold)             # Bold
bldred=${txtbld}$(tput setaf 1) #  red
bldblu=${txtbld}$(tput setaf 4) #  blue
bldwht=${txtbld}$(tput setaf 7) #  white
txtrst=$(tput sgr0)             # Reset
info=${bldwht}*${txtrst}        # Feedback
pass=${bldblu}*${txtrst}
warn=${bldred}*${txtrst}
ques=${bldblu}?${txtrst}

echo -e "Kernel Information: " `uname -smr`
echo -e "${COLOR_GRAY}`bash --version`"
echo -ne "${COLOR_GRAY}Uptime: "; uptime
echo -ne "${COLOR_GRAY}Server time is: "; date

source ~/dotfiles/.bash_gitprompt
